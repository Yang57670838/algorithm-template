function generateParenthesis(n) {
  const result = [];

  function generate(s, openCount, closeCount) {
    if (openCount === n && closeCount === n) {
      result.push(s);
      return;
    }
    if (openCount < n) {
      generate(s + "(", openCount + 1, closeCount);
    }
    if (closeCount < openCount) {
      generate(s + ")", openCount, closeCount + 1);
    }
  }

  generate("", 0, 0);
  return result;
}

console.log("result is", generateParenthesis(3));


