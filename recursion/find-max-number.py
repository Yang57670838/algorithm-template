def find_max_recursive(lst):
    if len(lst) == 1:  # Base case: Only one element left
        return lst[0]
    
    max_rest = find_max_recursive(lst[1:])  # Recursive call on the rest of the list
    return max(lst[0], max_rest)  # Compare the first element with max of the rest

numbers = [3, 8, 2, 10, 6, 4]
print(find_max_recursive(numbers))  # 10

