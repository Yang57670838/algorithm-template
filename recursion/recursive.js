function sumNumberArray(array) {

    if (array.length === 0) {
        return 0;
    }
    
    const firstElement = array[0];
    const restOfArray = array.slice(1);
    return firstElement + sum(restOfArray);
}

console.log(sum([1, 2, 3, 4, 5])); // Outputs: 15