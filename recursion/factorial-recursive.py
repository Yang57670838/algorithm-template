# 4! = 4 * 3 * 2 * 1 = 24
def factorial(n):
    assert n >= 0 and int(n) == n, ' The numbner must be position integer only.'
    if n == 1:
        return 1
    return n * factorial(n-1)

print(factorial(4))

