# fibonacci numbers 0, 1, 1, 2, 3 , 5, 8, 13, 21, 34, 55, 89, etc...
# f(n) = f(n - 1) + f(n - 2)
def find_fibonacci_number_by_index(n):
    assert n >= 0 and int(n) == n, ' The index must be position integer or zero only.'
    if n in [0, 1]:
        return n
    else:
        return find_fibonacci_number_by_index(n-1) = find_fibonacci_number_by_index(n-2)
    