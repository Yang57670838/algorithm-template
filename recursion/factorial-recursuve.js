function factorial(n) {
    // The base case: if n is 0, the factorial is 1
    if (n === 0) return 1;
  
    // Otherwise, the factorial is n * the factorial of n - 1
    return n * factorial(n - 1);
  }
  
  // Calculate the factorial of 5
  console.log(factorial(5)); // Outputs: 120